# Created 2019-08-07 Wed 14:39
#+TITLE: 
* Slack

- Reply to the *Welcome* message Nik posted to #zrh in Slack



* Directories (quiz)

- Files are stored in ...
  - Directories (boxes, places, stores)
- Directories are stored in ...
  - Directories (boxes, places, stores)
- There is one ... directory
  - root
- Filenames typically have two parts, the ... and the ...
  - basename, extension

* GitHub classroom

https://github.com/jfiksel/github-classroom-for-teachers

- Recommendation is that each assignment is its own repository

* TODO Command line

starts @ week 1 / day 2, s4

** Quiz

- The command to change directory is ...
  - cd
- The command to create a new directory is ...
  - mkdir
- The command line starts with a ... before you type
  - prompt
- The current directory is referenced as 
  - .
- The parent directory is referenced as
  - ..

* TODO Version control

starts @ week 1 / day 2, s19

** TODO Download and install git                                 :assignment:
** TODO Configure git                                            :assignment:

s24, "git config"



* TODO Check GitHub, clone, edit, add, commit, push              :assignment:


* Week 1 / day 1                                                 :assignment:
** TODO First HTML file                                          :assignment:

README.md

Create a new file in the same directory as this README.md file, called index.html.

Type the following, exactly as shown, in to the file.

<p>This is an HTML paragraph.</p>

Load the file in to your web browser, verify that you see something similar to the image.

** TODO Nesting elements                                         :assignment:

s31

** TODO Block and inline elements                                :assignment:

s34

** TODO A more complicated example                               :assignment:

s36

** TODO Images                                                   :assignment:

s44

** TODO Heading                                                  :assignment:

s52

** TODO Unordered lists                                          :assignment:

s53

** TODO Ordered lists                                            :assignment:

s55

** TODO div                                                      :assignment:

s57

** TODO span                                                     :assignment:

s59

** TODO Entities                                                 :assignment:

s61

** TODO Cheatsheet                                               :assignment:

s68

** TODO Recipes                                                  :assignment:

s68

** TODO Portfolio                                                :assignment:

s68



* Week 1 / day 2
** 
