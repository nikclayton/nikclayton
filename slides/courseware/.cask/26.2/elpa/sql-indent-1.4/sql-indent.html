<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title></title>
<meta name="author" content=""/>
<style type="text/css">
.underline { text-decoration: underline; }
</style>
<link rel="stylesheet" href="./reveal.js/css/reveal.css"/>

<link rel="stylesheet" href="./reveal.js/css/theme/moon.css" id="theme"/>


<!-- If the query includes 'print-pdf', include the PDF print sheet -->
<script>
    if( window.location.search.match( /print-pdf/gi ) ) {
        var link = document.createElement( 'link' );
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = './reveal.js/css/print/pdf.css';
        document.getElementsByTagName( 'head' )[0].appendChild( link );
    }
</script>
</head>
<body class='preso'>
<div class="reveal">
<div class="slides">
<section id="sec-title-slide">
<p class="date">Created: 2019-08-07 Wed 14:33</p>
</section>
<section id="table-of-contents-section">
<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#/slide-1">1. Overview</a></li>
<li><a href="#/slide-2">2. Installation</a></li>
<li><a href="#/slide-3">3. Customization</a>
<ul>
<li><a href="#/slide-3-1">3.1. A Simple Example</a></li>
<li><a href="#/slide-3-2">3.2. Customization Basics</a></li>
<li><a href="#/slide-3-3">3.3. Indentation control items</a>
<ul>
<li><a href="#/slide-3-3-1">3.3.1. Indentation Helper Functions</a></li>
<li><a href="#/slide-3-3-2">3.3.2. More Indentation Helper Functions</a></li>
</ul>
</li>
<li><a href="#/slide-3-4">3.4. Syntactic Symbols</a></li>
</ul>
</li>
<li><a href="#/slide-4">4. Limitations</a>
<ul>
<li><a href="#/slide-4-1">4.1. Parsing expressions</a></li>
</ul>
</li>
</ul>
</div>
</div>
</section>
<p>
sql-indent.el &#x2013; syntax based indentation for SQL files for GNU Emacs
</p>

<p>
<b>NOTE</b> This file is formatted as an Emacs Org file.  If you open it in GNU
Emacs, you will be able to open-close sections of the file, which will make
navigation easier.
</p>

<section>
<section id="slide-1">
<h2 id="1"><span class="section-number-2">1</span> Overview</h2>
<p>
sql-indent.el is a GNU Emacs minor mode which adds support for syntax-based
indentation when editing SQL code: TAB indents the current line based on the
syntax of the SQL code on previous lines.  This works like the indentation for
C and C++ code.
</p>

<p>
The package also defines align rules so that the <code>align</code> function works for
SQL statements, see <code>sqlind-align-rules</code> for which rules are defined.  This
can be used to align multiple lines around equal signs or "as" statements.
Here is an example of alignment rules:
</p>

<div class="org-src-container">

<pre  class="src src-sql"><span style="color: #F0DFAF; font-weight: bold;">update</span> my_table
   <span style="color: #F0DFAF; font-weight: bold;">set</span> col1_has_a_long_name = value1,
       col2_is_short        = value2,
       col3_med             = v2,
       c4                   = v5
 <span style="color: #F0DFAF; font-weight: bold;">where</span> cond1 <span style="color: #F0DFAF; font-weight: bold;">is</span> <span style="color: #F0DFAF; font-weight: bold;">not</span> <span style="color: #F0DFAF; font-weight: bold;">null</span>;

<span style="color: #F0DFAF; font-weight: bold;">select</span> long_colum <span style="color: #F0DFAF; font-weight: bold;">as</span> lcol,
       scol       <span style="color: #F0DFAF; font-weight: bold;">as</span> short_column,
       mcolu      <span style="color: #F0DFAF; font-weight: bold;">as</span> mcol,
  <span style="color: #F0DFAF; font-weight: bold;">from</span> my_table;
</pre>
</div>

<p>
To use this feature, select the region you want to align and type:
</p>

<div class="org-src-container">

<pre  class="src src-text">M-x align RET
</pre>
</div>

<p>
<code>sqlind-minor-mode</code> together with the align rules can assist in writing tidy
SQL code or formatting existing SQL code.  The indentation rules are
customizable, so you can adapt it to match your own indentation preferences.
</p>

</section>
</section>
<section>
<section id="slide-2">
<h2 id="2"><span class="section-number-2">2</span> Installation</h2>
<p>
To install this package, open the file <code>sql-indent.el</code> in Emacs and type
</p>

<div class="org-src-container">

<pre  class="src src-text">M-x package-install-from-buffer RET
</pre>
</div>

<p>
The syntax-based indentation of SQL code can be turned ON/OFF at any time by
enabling or disabling <code>sqlind-minor-mode</code>:
</p>

<div class="org-src-container">

<pre  class="src src-text">M-x sqlind-minor-mode RET
</pre>
</div>

<p>
To enable syntax-based indentation for every SQL buffer, you can add
<code>sqlind-minor-mode</code> to <code>sql-mode-hook</code>.  First, bring up the customization
buffer using the command:
</p>

<div class="org-src-container">

<pre  class="src src-text">M-x customize-variable RET sql-mode-hook RET
</pre>
</div>

<p>
Than, click on the "INS" button to add a new entry and put "sqlind-minor-mode"
in the text field.
</p>

</section>
</section>
<section>
<section id="slide-3">
<h2 id="3"><span class="section-number-2">3</span> Customization</h2>
<p>
The sql-indent.el package allows customizing the indentation rules to suit
your personal preferences or site specific coding styles.  To create a set of
customized rules, you will need to have basic familiarity with how indentation
works.  See also the "A Simple Example" section below and the
<code>sql-indent-left.el</code> file, which demonstrate how to set up custom indentation
rules.  The sections below contain the rest of the details.
</p>

<p>
The indentation process happens in two separate phases: first syntactic
information is determined about the line, than the line is indented based on
that syntactic information.  The syntactic parse is not expected to change
often, since it deals with the structure of the SQL code, however, indentation
is a personal preference, and can be easily customized.
</p>

<p>
Two variables control the way code is indented: <code>sqlind-basic-offset</code> and
<code>sqlind-indentation-offsets-alist</code>.  To customize these variables, you need to
create a function that sets custom values and add it to <code>sql-mode-hook</code>.
</p>

</section>
<section id="slide-3-1">
<h3 id="3-1"><span class="section-number-3">3.1</span> A Simple Example</h3>
<p>
The default indentation rules will align to the right all the keywords in a
SELECT statement, like this:
</p>

<div class="org-src-container">

<pre  class="src src-sql"><span style="color: #F0DFAF; font-weight: bold;">select</span> c1, c2
  <span style="color: #F0DFAF; font-weight: bold;">from</span> t1
 <span style="color: #F0DFAF; font-weight: bold;">where</span> c3 = 2
</pre>
</div>

<p>
If you prefer to have them aligned to the left, like this:
</p>

<div class="org-src-container">

<pre  class="src src-sql"><span style="color: #F0DFAF; font-weight: bold;">select</span> c1, c2
<span style="color: #F0DFAF; font-weight: bold;">from</span> t1
<span style="color: #F0DFAF; font-weight: bold;">where</span> c3 = 2
</pre>
</div>

<p>
You can add the following code to your init file:
</p>

<div class="org-src-container">

<pre  class="src src-emacs-lisp">(<span style="color: #F0DFAF; font-weight: bold;">require</span> '<span style="color: #BFEBBF;">sql-indent</span>)

<span style="color: #5F7F5F;">;; </span><span style="color: #7F9F7F;">Update indentation rules, select, insert, delete and update keywords</span>
<span style="color: #5F7F5F;">;; </span><span style="color: #7F9F7F;">are aligned with the clause start</span>

(<span style="color: #F0DFAF; font-weight: bold;">defvar</span> <span style="color: #DFAF8F;">my-sql-indentation-offsets-alist</span>
  `((select-clause 0)
    (insert-clause 0)
    (delete-clause 0)
    (update-clause 0)
    ,@sqlind-default-indentation-offsets-alist))

(add-hook 'sqlind-minor-mode-hook
    (<span style="color: #F0DFAF; font-weight: bold;">lambda</span> ()
       (<span style="color: #F0DFAF; font-weight: bold;">setq</span> sqlind-indentation-offsets-alist
             my-sql-indentation-offsets-alist)))
</pre>
</div>

</section>
<section id="slide-3-2">
<h3 id="3-2"><span class="section-number-3">3.2</span> Customization Basics</h3>
<p>
To customize indentation, you will need to provide a new value for the
<code>sqlind-indentation-offsets-alist</code> variable.  The variable is made buffer
local each time it is set, so you need to set it inside the <code>sql-mode-hook</code>.
The variable specifies how each syntactic symbol should be indented.  Since
only a few symbols need to be updated, the usual way to update it is to
"extend" the value of <code>sqlind-default-indentation-offsets-alist</code>, like so:
</p>

<div class="org-src-container">

<pre  class="src src-emacs-lisp">(<span style="color: #F0DFAF; font-weight: bold;">defvar</span> <span style="color: #DFAF8F;">my-sql-indentation-offsets-alist</span>
  `( <span style="color: #5F7F5F;">;; </span><span style="color: #7F9F7F;">put new syntactic symbols here, and add the default ones at the end.</span>
     <span style="color: #5F7F5F;">;; </span><span style="color: #7F9F7F;">If there is no value specified for a syntactic symbol, the default</span>
     <span style="color: #5F7F5F;">;; </span><span style="color: #7F9F7F;">will be picked up.</span>
    ,@sqlind-default-indentation-offsets-alist))

<span style="color: #5F7F5F;">;; </span><span style="color: #7F9F7F;">Arrange for the new indentation offset to be set up for each SQL buffer.</span>
(add-hook 'sqlind-minor-mode-hook
          (<span style="color: #F0DFAF; font-weight: bold;">lambda</span> ()
            (<span style="color: #F0DFAF; font-weight: bold;">setq</span> sqlind-indentation-offsets-alist
                  my-sql-indentation-offsets-alist)))
</pre>
</div>

<p>
The simplest way to adjust the indentation is to explore the syntactic
information using <code>sqlind-show-syntax-of-line</code>.  To use it, move the cursor to
the line you would like to indent and type:
</p>

<div class="org-src-container">

<pre  class="src src-text">M-x sqlind-show-syntax-of-line RET
</pre>
</div>

<p>
A message like the one below will be shown in the messages buffer:
</p>

<div class="org-src-container">

<pre  class="src src-text">((select-clause . 743) (statement-continuation . 743))
</pre>
</div>

<p>
The first symbol displayed is the syntactic symbol used for indentation, in
this case <code>select-clause</code>.  The syntactic symbols are described in a section
below, however, for now, this is the symbol that will need to be updated in
<code>sqlind-indentation-offsets-alist</code>.  The number next to it represents the
anchor, or reference position in the buffer where the current statement
starts.  The anchor and is useful if you need to write your own indentation
functions.
</p>

<p>
To customize indentation for this type of statement, add an entry in the
<code>sqlind-indentation-offsets-alist</code>, for the syntactic symbol shown, with
information about how it should be indented.  This information is a list
containing <b>indentation control items</b> (these are described below).
</p>

<p>
For example, to indent keyword in SELECT clauses at the same level as the
keyword itself, we use a number which is added to the indentation level of the
anchor, in this case, 0:
</p>

<div class="org-src-container">

<pre  class="src src-text">(select-clause 0)
</pre>
</div>

<p>
To indent it at <code>sqlind-basic-offset</code> plus one more space, use:
</p>

<div class="org-src-container">

<pre  class="src src-text">(select-clause + 1)
</pre>
</div>

<p>
To right-justify the keyword w.r.t the SELECT keyword, use:
</p>

<div class="org-src-container">

<pre  class="src src-text">(select-clause sqlind-right-justify-clause)
</pre>
</div>

<p>
The default value for <code>sqlind-indentation-offsets-alist</code> contains many
examples for indentation setup rules.
</p>

</section>
<section id="slide-3-3">
<h3 id="3-3"><span class="section-number-3">3.3</span> Indentation control items</h3>
<p>
<code>sqlind-calculate-indentation</code> is the function that calculates the indentation
offset to use, based on the contents of <code>sqlind-indentation-offsets-alist</code>.
The indentation offset starts with the indentation column of the ANCHOR point
and it is adjusted based on the following items:
</p>

<ul>
<li>a <code>NUMBER</code> &#x2013; the NUMBER will be added to the indentation offset.</li>

<li><code>+</code> &#x2013; the current indentation offset is incremented by
<code>sqlind-basic-offset</code></li>

<li><code>++</code> &#x2013; the current indentation offset is indentation by <code>2 *
   sqlind-basic-offset</code></li>

<li><code>-</code> &#x2013; the current indentation offset is decremented by
<code>sqlind-basic-offset</code></li>

<li><code>--</code> &#x2013; the current indentation offset is decremented by 2 *
<code>sqlind-basic-offset</code></li>

<li>a <code>FUNCTION</code> &#x2013; the syntax and current indentation offset is passed to the
function and its result is used as the new indentation offset.  This can be
used to further customize indentation.</li>

</ul>

</section>
<section id="slide-3-3-1">
<h4 id="3-3-1"><span class="section-number-4">3.3.1</span> Indentation Helper Functions</h4>
<p>
The following helper functions are available as part of the package and can be
used as the FUNCTION part in the <code>sqlind-indentation-offsets-alist</code>
</p>

<ol class="org-ol">
<li><a id="3-3-1-1"></a>sqlind-use-anchor-indentation<br />
<p>
discard the current offset and returns the indentation column of the ANCHOR
</p>
</li>

<li><a id="3-3-1-2"></a>sqlind-lineup-to-anchor<br />
<p>
discard the current offset and returns the column of the anchor point, which
may be different than the indentation column of the anchor point.
</p>
</li>

<li><a id="3-3-1-3"></a>sqlind-use-previous-line-indentation<br />
<p>
discard the current offset and returns the indentation column of the previous
line
</p>
</li>

<li><a id="3-3-1-4"></a>sqlind-lineup-open-paren-to-anchor<br />
<p>
if the line starts with an open parenthesis, discard the current
offset and return the column of the anchor point.
</p>
</li>

<li><a id="3-3-1-5"></a>sqlind-lone-semicolon<br />
<p>
if the line contains a single semicolon ';', use the value of
<code>sqlind-use-anchor-indentation</code>
</p>
</li>

<li><a id="3-3-1-6"></a>sqlind-adjust-operator<br />
<p>
if the line starts with an arithmetic operator (like <code>+</code> , <code>-</code>, or <code>||</code>), line
it up so that the right hand operand lines up with the left hand operand of
the previous line.  For example, it will indent the <code>||</code> operator like this:
</p>

<div class="org-src-container">

<pre  class="src src-sql"><span style="color: #F0DFAF; font-weight: bold;">select</span> col1, col2
          || col3 <span style="color: #F0DFAF; font-weight: bold;">as</span> composed_column, <span style="color: #7F9F7F;">-- align col3 with col2</span>
       col4
    || col5 <span style="color: #F0DFAF; font-weight: bold;">as</span> composed_column2
<span style="color: #F0DFAF; font-weight: bold;">from</span>   my_table
<span style="color: #F0DFAF; font-weight: bold;">where</span>  cond1 = 1
<span style="color: #F0DFAF; font-weight: bold;">and</span>    cond2 = 2;
</pre>
</div>
</li>

<li><a id="3-3-1-7"></a>sqlind-left-justify-logical-operator<br />
<p>
If the line starts with a logic operator (AND, OR NOT), line the operator with
the start of the WHERE clause.  This rule should be added to the
<code>in-select-clause</code> syntax after the <code>sqlind-lineup-to-clause-end</code> rule.
</p>
</li>

<li><a id="3-3-1-8"></a>sqlind-right-justify-logical-operator<br />
<p>
If the line starts with a logic operator (AND, OR NOT), line the operator with
the end of the WHERE clause. This rule should be added to the
<code>in-select-clause</code> syntax.
</p>

<div class="org-src-container">

<pre  class="src src-sql"><span style="color: #F0DFAF; font-weight: bold;">select</span> *
  <span style="color: #F0DFAF; font-weight: bold;">from</span> <span style="color: #F0DFAF; font-weight: bold;">table</span>
 <span style="color: #F0DFAF; font-weight: bold;">where</span> a = b
   <span style="color: #F0DFAF; font-weight: bold;">and</span> c = d; <span style="color: #7F9F7F;">-- AND clause sits under the where clause</span>
</pre>
</div>
</li>

<li><a id="3-3-1-9"></a>sqlind-adjust-comma<br />
<p>
if the line starts with a comma, adjust the current offset so that the line is
indented to the first word character.  For example, if added to a
<code>select-column</code> syntax indentation rule, it will indent as follows:
</p>

<div class="org-src-container">

<pre  class="src src-sql"><span style="color: #F0DFAF; font-weight: bold;">select</span> col1
   ,   col2 <span style="color: #7F9F7F;">-- align "col2" with "col1"</span>
<span style="color: #F0DFAF; font-weight: bold;">from</span> my_table;
</pre>
</div>
</li>

<li><a id="3-3-1-10"></a>sqlind-lineup-into-nested-statement<br />
<p>
discard the current offset and return the column of the first word inside a
nested statement.  This rule should be added to
<code>nested-statement-continuation</code> syntax indentation rule, and will indent as
follows:
</p>

<div class="org-src-container">

<pre  class="src src-sql">(    a,
     b  <span style="color: #7F9F7F;">-- b is aligned with a</span>
)
</pre>
</div>
</li>
</ol>

</section>
<section id="slide-3-3-2">
<h4 id="3-3-2"><span class="section-number-4">3.3.2</span> More Indentation Helper Functions</h4>
<p>
The following function contain indentation code specific to various SQL
statements.  Have a look at their doc strings for what they do:
</p>

<ul>
<li><code>sqlind-indent-comment-start</code>, <code>sqlind-indent-comment-continuation</code></li>

<li><code>sqlind-indent-select-column</code></li>

<li><code>sqlind-indent-select-table</code></li>

<li><code>sqlind-lineup-to-clause-end</code></li>

<li><code>sqlind-right-justify-clause</code></li>

<li><code>sqlind-lineup-joins-to-anchor</code></li>

</ul>

</section>
<section id="slide-3-4">
<h3 id="3-4"><span class="section-number-3">3.4</span> Syntactic Symbols</h3>
<p>
The SQL parsing code returns a syntax definition (either a symbol or a
list) and an anchor point, which is a buffer position.  The syntax symbols can
be used to define how to indent each line.  The following syntax symbols are
defined for SQL code:
</p>

<ul>
<li><code>(syntax-error MESSAGE START END)</code> &#x2013; this is returned when the parse
failed.  MESSAGE is an informative message, START and END are buffer
locations denoting the problematic region.  ANCHOR is undefined for this
syntax info</li>

<li><code>in-comment</code> &#x2013; line is inside a multi line comment, ANCHOR is the start of
the comment.</li>

<li><code>comment-start</code> &#x2013; line starts with a comment.  ANCHOR is the start of the
enclosing block.</li>

<li><code>in-string</code> &#x2013; line is inside a string, ANCHOR denotes the start of the
string.</li>

<li><code>toplevel</code> &#x2013; line is at toplevel (not inside any programming construct).
ANCHOR is usually (point-min).</li>

<li><code>(in-block BLOCK-KIND LABEL)</code> &#x2013; line is inside a block construct.
BLOCK-KIND (a symbol) is the actual block type and can be one of "if",
"case", "exception", "loop" etc.  If the block is labeled, LABEL contains
the label.  ANCHOR is the start of the block.</li>

<li><code>(in-begin-block KIND LABEL)</code> &#x2013; line is inside a block started by a begin
statement.  KIND (a symbol) is "toplevel-block" for a begin at toplevel,
"defun" for a begin that starts the body of a procedure or function,
\"package\" for a begin that starts the body of a package, nil for a begin
that is none of the previous.  For a "defun" or "package", LABEL is the
name of the procedure, function or package, for the other block types LABEL
contains the block label, or the empty string if the block has no label.
ANCHOR is the start of the block.</li>

<li><code>(block-start KIND)</code> &#x2013; line begins with a statement that starts a block.
KIND (a symbol) can be one of "then", "else" or "loop".  ANCHOR is the
reference point for the block start (the corresponding if, case, etc).</li>

<li><code>(block-end KIND LABEL)</code> &#x2013; the line contains an end statement.  KIND (a
symbol) is the type of block we are closing, LABEL (a string) is the block
label (or procedure name for an end defun).</li>

<li><code>declare-statement</code> &#x2013; line is after a declare keyword, but before the
begin.  ANCHOR is the start of the declare statement.</li>

<li><code>(package NAME)</code> &#x2013; line is inside a package definition.  NAME is the name
of the package, ANCHOR is the start of the package.</li>

<li><code>(package-body NAME)</code> &#x2013; line is inside a package body.  NAME is the name
of the package, ANCHOR is the start of the package body.</li>

<li><code>(create-statement WHAT NAME)</code> &#x2013; line is inside a CREATE statement (other
than create procedure or function).  WHAT is the thing being created, NAME
is its name.  ANCHOR is the start of the create statement.</li>

<li><code>(defun-start NAME)</code> &#x2013; line is inside a procedure of function definition
but before the begin block that starts the body.  NAME is the name of the
procedure/function, ANCHOR is the start of the procedure/function
definition.</li>

</ul>

<p>
The following SYNTAX-es are for SQL statements.  For all of them ANCHOR points
to the start of a statement itself.
</p>

<ul>
<li><code>labeled-statement-start</code> &#x2013; line is just after a label.</li>

<li><code>statement-continuation</code> &#x2013; line is inside a statement which starts on a
previous line.</li>

<li><code>nested-statement-open</code> &#x2013; line is just inside an opening bracket, but the
actual bracket is on a previous line.</li>

<li><code>nested-statement-continuation</code> &#x2013; line is inside an opening bracket, but
not the first element after the bracket.</li>

<li><code>nested-statement-close</code> line is inside an opening bracket and the line
contains the closing bracket as the first character.</li>

</ul>

<p>
The following SYNTAX-es are for statements which are SQL code (DML
statements).  They are specializations on the previous statement syntaxes and
for all of them a previous generic statement syntax is present earlier in the
SYNTAX list.  Unless otherwise specified, ANCHOR points to the start of the
clause (select, from, where, etc) in which the current point is.
</p>

<ul>
<li><code>with-clause</code> &#x2013; line is inside a WITH clause, but before the main SELECT
clause.</li>

<li><code>with-clause-cte</code> &#x2013; line is inside a with clause before a CTE (common
table expression) declaration</li>

<li><code>with-clause-cte-cont</code> &#x2013; line is inside a with clause before a CTE
definition</li>

<li><code>case-clause</code> &#x2013; line is on a CASE expression (WHEN or END clauses).
ANCHOR is the start of the CASE expression.</li>

<li><code>case-clause-item</code> &#x2013; line is on a CASE expression (THEN and ELSE clauses).
ANCHOR is the position of the case clause.</li>

<li><code>case-clause-item-cont</code> &#x2013; line is on a CASE expression but not on one of
the CASE sub-keywords.  ANCHOR points to the case keyword that this line is
a continuation of.</li>

<li><code>select-clause</code> &#x2013; line is inside a select statement, right before one of
its clauses (from, where, order by, etc).</li>

<li><code>select-column</code> &#x2013; line is inside the select column section, after a full
column was defined (and a new column definition is about to start).</li>

<li><code>select-column-continuation</code> &#x2013; line is inside the select column section,
but in the middle of a column definition.  The defined column starts on a
previous like.  Note that ANCHOR still points to the start of the select
statement itself.</li>

<li><code>select-join-condition</code> &#x2013; line is right before or just after the ON clause
for an INNER, LEFT or RIGHT join.  ANCHOR points to the join statement for
which the ON is defined.</li>

<li><code>select-table</code> &#x2013; line is inside the from clause, just after a table was
defined and a new one is about to start.</li>

<li><code>select-table-continuation</code> &#x2013; line is inside the from clause, inside a
table definition which starts on a previous line. Note that ANCHOR still
points to the start of the select statement itself.</li>

<li><code>(in-select-clause CLAUSE)</code> &#x2013; line is inside the select CLAUSE, which can
be "where", "order by", "group by" or "having".  Note that CLAUSE can never
be "select" and "from", because we have special syntaxes inside those
clauses.</li>

<li><code>insert-clause</code> &#x2013; line is inside an insert statement, right before one of
its clauses (values, select).</li>

<li><code>(in-insert-clause CLAUSE)</code> &#x2013; line is inside the insert CLAUSE, which can
be "insert into" or "values".</li>

<li><code>delete-clause</code> &#x2013; line is inside a delete statement right before one of
its clauses.</li>

<li><code>(in-delete-clause CLAUSE)</code> &#x2013; line is inside a delete CLAUSE, which can be
"delete from" or "where".</li>

<li><code>update-clause</code> &#x2013; line is inside an update statement right before one of
its clauses.</li>

<li><code>(in-update-clause CLAUSE)</code> &#x2013; line is inside an update CLAUSE, which can
be "update", "set" or "where"</li>

</ul>
</section>
</section>
<section>
<section id="slide-4">
<h2 id="4"><span class="section-number-2">4</span> Limitations</h2>
<div class="outline-text-2" id="text-4">
</div>
</section>
<section id="slide-4-1">
<h3 id="4-1"><span class="section-number-3">4.1</span> Parsing expressions</h3>
<p>
There is no support for parsing SQL expressions, so if an expression is broken
over several lines, sql-indent.el will consider all lines to be
<code>statement-continuation</code> lines.  The exception is that bracketed expressions
are identified correctly so they can be used for indentation.
</p>

<p>
The examples below summarize what is supported and what is not, as well as the
workarounds:
</p>

<div class="org-src-container">

<pre  class="src src-sql"><span style="color: #7F9F7F;">-- SUPPORTED: case expression immediately after assignment</span>
var := <span style="color: #F0DFAF; font-weight: bold;">case</span> ind
       <span style="color: #F0DFAF; font-weight: bold;">when</span> 1 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Guy'</span>
       <span style="color: #F0DFAF; font-weight: bold;">when</span> 2 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Abc'</span>
       <span style="color: #F0DFAF; font-weight: bold;">when</span> 3 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Def'</span>
       <span style="color: #F0DFAF; font-weight: bold;">else</span> <span style="color: #CC9393;">'World'</span>
       <span style="color: #F0DFAF; font-weight: bold;">end</span> <span style="color: #F0DFAF; font-weight: bold;">case</span>;

<span style="color: #7F9F7F;">-- NOT SUPPORTED: any complex expression involving a case expression.  entire</span>
<span style="color: #7F9F7F;">-- expression is a 'statement-continuation</span>
var := <span style="color: #CC9393;">'abc'</span>
  || <span style="color: #F0DFAF; font-weight: bold;">case</span> ind
  <span style="color: #F0DFAF; font-weight: bold;">when</span> 1 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Guy'</span>
  <span style="color: #F0DFAF; font-weight: bold;">when</span> 2 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Abc'</span>
  <span style="color: #F0DFAF; font-weight: bold;">when</span> 3 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Def'</span>
  <span style="color: #F0DFAF; font-weight: bold;">else</span> <span style="color: #CC9393;">'World'</span>
  <span style="color: #F0DFAF; font-weight: bold;">end</span> <span style="color: #F0DFAF; font-weight: bold;">case</span>;

<span style="color: #7F9F7F;">-- WORKAROUND: use brackets instead</span>
var := <span style="color: #CC9393;">'abc'</span>
  || (<span style="color: #F0DFAF; font-weight: bold;">case</span> ind
      <span style="color: #F0DFAF; font-weight: bold;">when</span> 1 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Guy'</span>
      <span style="color: #F0DFAF; font-weight: bold;">when</span> 2 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Abc'</span>
      <span style="color: #F0DFAF; font-weight: bold;">when</span> 3 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Def'</span>
      <span style="color: #F0DFAF; font-weight: bold;">else</span> <span style="color: #CC9393;">'World'</span>
      <span style="color: #F0DFAF; font-weight: bold;">end</span> <span style="color: #F0DFAF; font-weight: bold;">case</span>);

<span style="color: #7F9F7F;">-- SUPPORTED: case expression as select column</span>
<span style="color: #F0DFAF; font-weight: bold;">select</span> col1,
       <span style="color: #F0DFAF; font-weight: bold;">case</span> ind
       <span style="color: #F0DFAF; font-weight: bold;">when</span> 1 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Guy'</span>
       <span style="color: #F0DFAF; font-weight: bold;">when</span> 2 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Abc'</span>
       <span style="color: #F0DFAF; font-weight: bold;">when</span> 3 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Def'</span>
       <span style="color: #F0DFAF; font-weight: bold;">else</span> <span style="color: #CC9393;">'World'</span>
       <span style="color: #F0DFAF; font-weight: bold;">end</span> <span style="color: #F0DFAF; font-weight: bold;">case</span>,
       col2,
  <span style="color: #F0DFAF; font-weight: bold;">from</span> some_table;

<span style="color: #7F9F7F;">-- NOT SUPPORTED: any complex expression involving a case expression in a</span>
<span style="color: #7F9F7F;">-- select column.  Entire column is a 'select-column-continuation</span>
<span style="color: #F0DFAF; font-weight: bold;">select</span> col1,
       <span style="color: #CC9393;">'abc'</span> || <span style="color: #F0DFAF; font-weight: bold;">case</span> ind
         <span style="color: #F0DFAF; font-weight: bold;">when</span> 1 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Guy'</span>
         <span style="color: #F0DFAF; font-weight: bold;">when</span> 2 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Abc'</span>
         <span style="color: #F0DFAF; font-weight: bold;">when</span> 3 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Def'</span>
         <span style="color: #F0DFAF; font-weight: bold;">else</span> <span style="color: #CC9393;">'World'</span>
         <span style="color: #F0DFAF; font-weight: bold;">end</span> <span style="color: #F0DFAF; font-weight: bold;">case</span>,
       col2,
  <span style="color: #F0DFAF; font-weight: bold;">from</span> some_table;

<span style="color: #7F9F7F;">-- WORKAROUND: use brackets instead</span>
<span style="color: #F0DFAF; font-weight: bold;">select</span> col1,
       <span style="color: #CC9393;">'abc'</span> || (<span style="color: #F0DFAF; font-weight: bold;">case</span> ind
                 <span style="color: #F0DFAF; font-weight: bold;">when</span> 1 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Guy'</span>
                 <span style="color: #F0DFAF; font-weight: bold;">when</span> 2 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Abc'</span>
                 <span style="color: #F0DFAF; font-weight: bold;">when</span> 3 <span style="color: #F0DFAF; font-weight: bold;">then</span> <span style="color: #CC9393;">'Def'</span>
                 <span style="color: #F0DFAF; font-weight: bold;">else</span> <span style="color: #CC9393;">'World'</span>
                 <span style="color: #F0DFAF; font-weight: bold;">end</span> <span style="color: #F0DFAF; font-weight: bold;">case</span>),
       col2,
  <span style="color: #F0DFAF; font-weight: bold;">from</span> some_table;
</pre>
</div>

<p>
.
</p>
</section>
</section>
</div>
</div>
<script src="./reveal.js/lib/js/head.min.js"></script>
<script src="./reveal.js/js/reveal.js"></script>
<script>
// Full list of configuration options available here:
// https://github.com/hakimel/reveal.js#configuration
Reveal.initialize({

controls: true,
progress: true,
history: false,
center: true,
slideNumber: 'c',
rollingLinks: false,
keyboard: true,
mouseWheel: false,
fragmentInURL: false,
hashOneBasedIndex: false,
pdfSeparateFragments: true,

overview: true,

theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
transition: Reveal.getQueryHash().transition || 'convex', // see README of reveal.js for options
transitionSpeed: 'default',

// Optional libraries used to extend on reveal.js
dependencies: [
 { src: './reveal.js/lib/js/classList.js', condition: function() { return !document.body.classList; } },
 { src: './reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
 { src: './reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
 { src: './reveal.js/plugin/zoom-js/zoom.js', async: true, condition: function() { return !!document.body.classList; } },
 { src: './reveal.js/plugin/notes/notes.js', async: true, condition: function() { return !!document.body.classList; } }]

});
</script>
</body>
</html>
